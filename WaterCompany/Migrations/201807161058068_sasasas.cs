namespace WaterCompany.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class sasasas : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "UserType", c => c.Boolean(nullable: false));
            AddColumn("dbo.AspNetUsers", "UserId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "UserId");
            DropColumn("dbo.AspNetUsers", "UserType");
        }
    }
}
