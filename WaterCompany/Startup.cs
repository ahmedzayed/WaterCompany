﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WaterCompany.Startup))]
namespace WaterCompany
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
