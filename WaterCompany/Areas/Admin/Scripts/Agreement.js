﻿var Agreement = {
    init: function () {
        Search();
    },
}
function Search() {

    var $form = $("#SearchForm");

    var data = new FormData();
    var formData = $form.serializeArray();
    $.each(formData, function (key, value) {
        data.append(this.name, this.value);
    });

    ajaxRequest('Post', $("#SearchForm").attr('action'), data, 'html', false, false).done(function (result) {
        $("#SearchTableContainer").html(result);
    });
}
function SearchAll() {
    var $form = $("#SearchForm");

    var data = new FormData();

    $('#Name').val("");
    $('#Id').val("");
    $('#PositionIdSm').val("0");

    var formData = $form.serializeArray();
    //$.each(formData, function (key, value) {
    //    data.append(this.name, this.value);
    //});

    ajaxRequest('Post', $("#SearchForm").attr('action'), data, 'html', false, false).done(function (result) {
        $("#SearchTableContainer").html(result);
    });
}

function Save() {
    //alert("1ddd")
    var $form = $("#ModelForm");
    //if ($form.valid()) {
    var data = new FormData();
    var formData = $form.serializeArray();
    $.each(formData, function (key, value) {
        data.append(this.name, this.value);
    });

    if (IsValid()) {
        ajaxRequest($("#ModelForm").attr('method'), $("#ModelForm").attr('action'), data, 'json', false, false).done(function (result) {
            //alert("ddd")
            var res = result.split(',');
            //debugger;
            if (res[0] == "success") {
                $('#myModalAddEdit').modal('hide');
                toastr.success(res[1]);
                window.Location = "../Agreement/Index";
                //Search();
                //Clear();


            }
            else
                toastr.error(res[1]);
        });
    }
    //}
}
//function Create() {
//    var Url = "../Agreement/Create";
//    //$('#myModalAddEdit').load(Url, function (response, status, xhr) {
//    //    $('#myModalAddEdit').modal('show');
//    //    SearchProduct();
//    //});
//    //window.Location = "../Agreement/Index";
//}
function Edit(id) {

    var Url = "../Agreement/Edit?id=" + id;
    //$('#myModalAddEdit').load(Url, function (response, status, xhr) {
    //    $('#myModalAddEdit').modal('show');
    //});

    window.Location = Url;
}
function Delete(id) {
    var Url = "../Agreement/Delete?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}
function DeleteRow(id) {
    var Url = "../Agreement/DeleteRow?Id=" + id;
    ajaxRequest("Post", Url, "", 'json', false, false).done(function (result) {
        var res = result.split(',');
        if (res[0] == "success") {
            $('#myModalAddEdit').modal('hide');
            Search();
        }
        else
            toastr.error(res[1]);
    });
}
function IsValid() {
    var isValidItem = true;

    if ($('#NameAr').val() == "") {
        isValidItem = false;
        toastr.error("من فضلك ادخل الاسم");
    }
    if ($('#NameEn').val() == "") {
        isValidItem = false;
        toastr.error("من فضلك ادخل الاسم انجليزي");
    }
    if ($('#PositionId').val() == "0") {
        isValidItem = false;
        toastr.error("من فضلك ادخل المنصب");
    }
    if ($('#UserName').val() == "") {
        isValidItem = false;
        toastr.error("من فضلك ادخل اسم المستخدم");
    }
    if ($('#Password').val() == "") {
        isValidItem = false;
        toastr.error("من فضلك ادخل كلمة المرور");
    }



    return isValidItem;
}
function Clear() {
    $('#NameAr').val() = "";
    $('#NameEn').val() = "";
    $('#PositionId').val() = "0";
    $('#Address').val() = "";
    $('#UserName').val() = "";
    $('#Password').val() = "";
    $('#Telephone').val() = "";
    $('#Note').val() = "";
}

///////////////////////Cahnge Product////////////////////////

function ProductChange() {
    $("#ModelForm").delegate("#ProductId", "change", function () {

        alert("ddd")
        var selectedType = $('#ProductId').val();
        $.ajax({
            type: "POST",
            data: { "productId": selectedType },
            url: "../Agreement/getProductPrice",
            dataType: 'json',
            success: function (data) {
                $('#Price').val(data);
            }
        });
    });
}

////////////////////////Add Product///////////////////////////////
function SearchProduct(RequestId) {
    if (RequestId != '')
        var url = "../SearchProduct?RequestId=" + RequestId;
    else
        var url = "../Agreement/SearchProduct?RequestId=" + RequestId;


    ajaxRequest("post", url, "", 'html').done(function (data) {
        $("#SearchTableContainerProduct").html(data);
    });
}
function AddProduct() {
    if (IsValidProduct()) {

        var ProductName = $("#ProductId option:selected").text();
        var ProductId = $("#ProductId").val();
        var Price = $("#Price").val();
        var Target = $("#Target").val();


        var Url = "../Agreement/AddAgreementProduct?ProductName=" + ProductName + "&ProductId=" + ProductId + "&Price=" + Price + "&Target=" + Target;

        ajaxRequest("Post", Url, "", 'json', false, false).done(function (result) {
            var res = result.split(',');
            if (res[0] == "success") {
                toastr.success(res[1]);
                SearchProduct(0);
                $("#ProductId").val("0");
                $("#Price").val("");
                $("#Target").val("");
            }
            else
                toastr.error(res[1]);
        });
    }

}

function IsValidProduct() {
    var isValidItem = true;

    if ($('#ProductId').val() == "0") {
        isValidItem = false;
        toastr.warning("من فضلك اختر المنتج");
    }
    if ($('#Price').val() == "0" || $('#Price').val() == "") {
        isValidItem = false;
        toastr.warning("من فضلك ادخل السعر");
    }

    if ($('#Target').val() == "") {
        isValidItem = false;
        toastr.warning("من فضلك ادخل الهدف المبيعي");
    }

    return isValidItem;
}

function DeleteProduct(id) {


    var Url = "../Agreement/DeleteAgreementProduct?Id=" + id;

    ajaxRequest("Post", Url, "", 'json', false, false).done(function (result) {
        var res = result.split(',');
        if (res[0] == "success") {
            toastr.success(res[1]);
            SearchProduct();
        }
        else
            toastr.error(res[1]);
    });
}

////////////////////////Add Product///////////////////////////////
function SearchProductEdit(RequestId) {

    var url = "../SearchProductEdit?RequestId=" + RequestId;



    ajaxRequest("post", url, "", 'html').done(function (data) {
        $("#SearchTableContainerProduct").html(data);
    });
}
function AddProductEdit() {
    if (IsValidProduct()) {

        var ProductName = $("#ProductId option:selected").text();
        var ProductId = $("#ProductId").val();
        var Price = $("#Price").val();
        var Target = $("#Target").val();
        var Id = $("#Id").val();


        var Url = "../AddAgreementProductEdit?AgreementId=" + Id + "&ProductId=" + ProductId + "&Price=" + Price + "&Target=" + Target;

        ajaxRequest("Post", Url, "", 'json', false, false).done(function (result) {
            var res = result.split(',');
            if (res[0] == "success") {
                toastr.success(res[1]);
                SearchProductEdit(Id);
                $("#ProductId").val("0");
                $("#Price").val("");
                $("#Target").val("");
            }
            else
                toastr.error(res[1]);
        });
    }

}

function DeleteProductEdit(id) {

    var Id = $("#Id").val();

    var Url = "../DeleteAgreementProductEdit?Id=" + id;

    ajaxRequest("Post", Url, "", 'json', false, false).done(function (result) {
        var res = result.split(',');
        if (res[0] == "success") {
            toastr.success(res[1]);
            SearchProductEdit(Id);
        }
        else
            toastr.error(res[1]);
    });
}




