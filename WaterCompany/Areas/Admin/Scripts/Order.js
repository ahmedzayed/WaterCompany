﻿var Order = {
    init: function () {
        Search();
    },
}

function Search() {

    var $form = $("#SearchForm");

    var data = new FormData();
    var formData = $form.serializeArray();
    $.each(formData, function (key, value) {
        data.append(this.name, this.value);
    });

    ajaxRequest('Post', $("#SearchForm").attr('action'), data, 'html', false, false).done(function (result) {
        $("#SearchTableContainer").html(result);
    });
}
//function SearchAll() {
//    var $form = $("#SearchForm");

//    var data = new FormData();

//    $('#NameSm').val("");


//    var formData = $form.serializeArray();

//    ajaxRequest('Post', $("#SearchForm").attr('action'), data, 'html', false, false).done(function (result) {
//        $("#SearchTableContainer").html(result);
//    });
//}

function Save() {

    var $form = $("#ModelForm");
    //if ($form.valid()) {
    var data = new FormData();
    var formData = $form.serializeArray();
    $.each(formData, function (key, value) {
        data.append(this.name, this.value);
    });

    if (IsValid()) {
        ajaxRequest($("#ModelForm").attr('method'), $("#ModelForm").attr('action'), data, 'json', false, false).done(function (result) {

            var res = result.split(',');
            //debugger;
            if (res[0] == "success") {
                $('#myModalAddEdit').modal('hide');
                toastr.success(res[1]);
                Search();
                Clear();


            }
            else
                toastr.error(res[1]);
        });
    }
    //}
}

function Create() {
    var Url = "../Order/Create";
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}
function Edit(id) {
    var Url = "../Order/Edit?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}
function Delete(id) {
    var Url = "../Order/Delete?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}
function DeleteRow(id) {
    var Url = "../Order/DeleteRow?Id=" + id;
    ajaxRequest("Post", Url, "", 'json', false, false).done(function (result) {
        var res = result.split(',');
        if (res[0] == "success") {
            $('#myModalAddEdit').modal('hide');
            Search();
        }
        else
            toastr.error(res[1]);
    });
}
function IsValid() {
    var isValidItem = true;

    if ($('#NameAr').val() == "") {
        isValidItem = false;
        toastr.error("من فضلك ادخل اسم عربي");
    }
    if ($('#NameEn').val() == "") {
        isValidItem = false;
        toastr.error("من فضلك ادخل اسم انجليزي");
    }
    if ($('#Price').val() == "" || $('#Price').val() == "0.0" || $('#Price').val() == "0") {
        isValidItem = false;
        toastr.error("من فضلك ادخل اسم انجليزي");
    }


    return isValidItem;
}
function Clear() {
    $('#NameAr').val() = "";
    $('#NameEn').val() = "";
    $('#DescAr').val() = "";
    $('#DescAr').val() = "";
    $('#Price').val() = "0";
    $('#Note').val() = "";
}




