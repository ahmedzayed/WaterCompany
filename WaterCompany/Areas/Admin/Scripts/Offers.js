﻿var Offers = {
    init: function () {
        Search();
        ProductChange();
    },
}

function Search() {

    var $form = $("#SearchForm");

    var data = new FormData();
    var formData = $form.serializeArray();
    $.each(formData, function (key, value) {
        data.append(this.name, this.value);
    });

    ajaxRequest('Post', $("#SearchForm").attr('action'), data, 'html', false, false).done(function (result) {
        $("#SearchTableContainer").html(result);
    });
}
function SearchAll() {
    var $form = $("#SearchForm");

    var data = new FormData();

    $('#Name').val("");
    $('#Id').val("");
    $('#PositionIdSm').val("0");

    var formData = $form.serializeArray();
    //$.each(formData, function (key, value) {
    //    data.append(this.name, this.value);
    //});

    ajaxRequest('Post', $("#SearchForm").attr('action'), data, 'html', false, false).done(function (result) {
        $("#SearchTableContainer").html(result);
    });
}

function Save() {
    var $form = $("#ModelForm");
    //if ($form.valid()) {
    var data = new FormData();
    var formData = $form.serializeArray();
    $.each(formData, function (key, value) {
        data.append(this.name, this.value);
    });

    if (IsValid()) {
        ajaxRequest($("#ModelForm").attr('method'), $("#ModelForm").attr('action'), data, 'json', false, false).done(function (result) {
            //alert("ddd")
            var res = result.split(',');
            //debugger;
            if (res[0] == "success") {
                //$('#myModalAddEdit').modal('hide');
                toastr.success(res[1]);
                //window.location.reload();
                //window.Location = "../Invoice/Index";
                //Search();
                // Clear();


            }
            else
                toastr.error(res[1]);
        });
    }
    //}
}
function Create() {
    var Url = "../Invoice/Create";
    //$('#myModalAddEdit').load(Url, function (response, status, xhr) {
    //    $('#myModalAddEdit').modal('show');
    //    SearchProduct();
    //});
    window.Location = "../Invoice/Create";
}
//function Edit(id) {
//    alert(id)
//    var Url = "../Offers/Edit?id=" + id;
//    //$('#myModalAddEdit').load(Url, function (response, status, xhr) {
//    //    $('#myModalAddEdit').modal('show');
//    //});

//    //window.Location = Url;
//}
function Delete(id) {
    var Url = "../Offers/Delete?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}
function DeleteRow(id) {
    var Url = "../Offers/DeleteRow?Id=" + id;
    ajaxRequest("Post", Url, "", 'json', false, false).done(function (result) {
        var res = result.split(',');
        if (res[0] == "success") {
            $('#myModalAddEdit').modal('hide');
            Search();
        }
        else
            toastr.error(res[1]);
    });
}
function IsValid() {
    var isValidItem = true;

    if ($('#DelegateId').val() == "0") {
        isValidItem = false;
        toastr.error("من فضلك ادخل المندوب");
    }
    if ($('#DriverId').val() == "0") {
        isValidItem = false;
        toastr.error("من فضلك ادخل السائق");
    }
    if ($('#TypeId').val() == "0") {
        isValidItem = false;
        toastr.error("من فضلك ادخل نوع الفاتورة");
    }
    if ($('#PaymentId').val() == "0") {
        isValidItem = false;
        toastr.error("من فضلك ادخل طريقة الدفع");
    }

    return isValidItem;
}
function Clear() {
    $('#NameAr').val() = "";
    $('#NameEn').val() = "";
    $('#PositionId').val() = "0";
    $('#Address').val() = "";
    $('#UserName').val() = "";
    $('#Password').val() = "";
    $('#Telephone').val() = "";
    $('#Note').val() = "";
}


function ProductChange() {
    $("#ModelForm").delegate("#ProductId", "change", function () {

        alert("ddd")
        var selectedType = $('#ProductId').val();
        $.ajax({
            type: "POST",
            data: { "productId": selectedType },
            url: "../Invoice/getProductPrice",
            dataType: 'json',
            success: function (data) {
                $('#Price').val(data);
            }
        });
    });
}

function SearchProduct(RequestId) {
    if (RequestId != '')
        var url = "../SearchProduct?RequestId=" + RequestId;
    else
        var url = "../Offers/SearchProduct?RequestId=" + RequestId;


    ajaxRequest("post", url, "", 'html').done(function (data) {
        $("#SearchTableContainerProduct").html(data);
    });
}
function AddProduct() {
    if (IsValidProduct()) {

        var ProductName = $("#ProductId option:selected").text();
        var ProductId = $("#ProductId").val();
        var Price = $("#Price").val();
        var Quantity = $("#Quantity").val();
        var InvoiceId = $("#InvoiceId").val();

        if (InvoiceId != '' && InvoiceId != '0')
            var Url = "../Offers/AddAgreementProduct?ProductName=" + ProductName + "&ProductId=" + ProductId + "&Price=" + Price + "&Quantity=" + Quantity;
        else
            var Url = "../Offers/AddAgreementProduct?ProductName=" + ProductName + "&ProductId=" + ProductId + "&Price=" + Price + "&Quantity=" + Quantity;


        ajaxRequest("Post", Url, "", 'json', false, false).done(function (result) {
            var res = result.split(',');
            if (res[0] == "success") {
                toastr.success(res[1]);
                SearchProduct(0);
                $("#ProductId").val("0");
                $("#Price").val("");
                $("#Quantity").val("");
            }
            else
                toastr.error(res[1]);
        });
    }

}

function IsValidProduct() {
    var isValidItem = true;

    if ($('#ProductId').val() == "0") {
        isValidItem = false;
        toastr.warning("من فضلك اختر المنتج");
    }
    if ($('#Price').val() == "0" || $('#Price').val() == "") {
        isValidItem = false;
        toastr.warning("من فضلك ادخل السعر");
    }



    return isValidItem;
}

function DeleteProduct(id) {


    var Url = "../Offers/DeleteInvoiceProduct?Id=" + id;

    ajaxRequest("Post", Url, "", 'json', false, false).done(function (result) {
        var res = result.split(',');
        if (res[0] == "success") {
            toastr.success(res[1]);
            SearchProduct(0);
        }
        else
            toastr.error(res[1]);
    });
}




function SearchProductEdit(RequestId) {
    if (RequestId != '')
        var url = "../SearchProductEdit?RequestId=" + RequestId;
    else
        var url = "../Offers/SearchProductEdit?RequestId=" + RequestId;


    ajaxRequest("post", url, "", 'html').done(function (data) {
        $("#SearchTableContainerProduct").html(data);
    });
}
function AddProductEdit() {
    alert("aaa");
    if (IsValidProduct()) {

        var ProductId = $("#ProductId").val();
        var Price = $("#Price").val();
        var Quantity = $("#Quantity").val();
        var InvoiceId = $("#InvoiceId").val();

        if (InvoiceId != '' && InvoiceId != '0')
            var Url = "../AddInvoiceProductEdit?InvoiceId=" + InvoiceId + "&ProductId=" + ProductId + "&Price=" + Price + "&Quantity=" + Quantity;
        else
            var Url = "../Offers/AddInvoiceProductEdit?InvoiceId=" + InvoiceId + "&ProductId=" + ProductId + "&Price=" + Price + "&Quantity=" + Quantity;


        ajaxRequest("Post", Url, "", 'json', false, false).done(function (result) {
            var res = result.split(',');
            if (res[0] == "success") {
                toastr.success(res[1]);
                SearchProductEdit(InvoiceId);
                $("#ProductId").val("0");
                $("#Price").val("");
                $("#Quantity").val("");
            }
            else
                toastr.error(res[1]);
        });
    }

}

function DeleteProductEdit(id) {

    var InvoiceId = $("#InvoiceId").val();
    var Url = "../DeleteInvoiceProductEdit?Id=" + id;

    ajaxRequest("Post", Url, "", 'json', false, false).done(function (result) {
        var res = result.split(',');
        if (res[0] == "success") {
            toastr.success(res[1]);
            SearchProductEdit(InvoiceId);
        }
        else
            toastr.error(res[1]);
    });
}
