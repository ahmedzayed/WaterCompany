﻿using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WaterCompany.Core;
using WaterCompany.Models;
using WaterCompanyViewModel.Admin;

namespace WaterCompany.Areas.Admin.Controllers
{
    public class CustomersController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        // GET: Admin/Customers
        public ActionResult Index()
        {
            //var model = "ProductsSelectAll".ExecuParamsSqlOrStored(false).AsList<CustomersVM>();
            return View();
        }
        public ActionResult Search()
        {
            var model = "CustomerssSelect".ExecuParamsSqlOrStored(false).AsList<CustomerVM>();

            return PartialView(model);
        }
        [HttpGet]
        public ActionResult Create()
        {
            var lst = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst.AddRange("Sp_RegionDrop".ExecuParamsSqlOrStored(false).AsList<RegionVM>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.RegionList = lst;

            CustomerFM obj = new CustomerFM();
            return PartialView("~/Areas/Admin/Views/Customers/Create.cshtml", obj);
        }
        [HttpGet]
        public ActionResult Edit(int Id)
        {
            var lst = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst.AddRange("Sp_RegionDrop".ExecuParamsSqlOrStored(false).AsList<RegionVM>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.RegionList = lst;
            var model = "CustomerssSelectById".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<CustomerFM>();
            return PartialView("~/Areas/Admin/Views/Customers/Create.cshtml", model.FirstOrDefault());
        }
        [HttpPost]

        public async Task<ActionResult> Create(CustomerFM model)
        {




            var data = "CustomerssInsert".ExecuParamsSqlOrStored(false, "Name".KVP(model.Name),
                "Longid".KVP(model.Longid), "Latid".KVP(model.Latid),
                "Area".KVP("asasas"), 
                "RegionId".KVP(model.RegionId),
                "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {
                if (model.Id == 0)
                {
                    var modeluser = "CustomerssSelect".ExecuParamsSqlOrStored(false).AsList<CustomerVM>();
                int userid = modeluser.LastOrDefault().Id;
                var user = new ApplicationUser { UserName = model.UserName, Email = "asas"+userid+"@yahoo.com", UserType = false, UserId = userid };
                var result = await UserManager.CreateAsync(user, model.Password);
                
                    if (result.Succeeded)
                    {

                        // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=320771
                        // Send an email with this link
                        // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                        // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                        // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

                        return Json("success," + "Add Success", JsonRequestBehavior.AllowGet);


                    }
                    
                }
                else
                {
                    return Json("success," + "Update Success", JsonRequestBehavior.AllowGet);

                }

            }
            return Json("error," + " Error", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int Id)
        {
            //var data = "SP_DeleteCategory".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            CustomerVM obj = new CustomerVM();
            obj.Id = Id;
            return PartialView("~/Areas/Admin/Views/Customers/Delete.cshtml", obj);



        }
        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "CustomerssDelete".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();

            if (data == 1)
                return Json("success," + "Delete Success", JsonRequestBehavior.AllowGet);
            else
                return Json("error," + "Error", JsonRequestBehavior.AllowGet);
        }
        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
    }
}