﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WaterCompany.Core;
using WaterCompanyViewModel.Admin;

namespace WaterCompany.Areas.Admin.Controllers
{
    public class ProductController : Controller
    {
        // GET: Admin/Product
        public ActionResult Index()
        {
            //var model = "ProductsSelectAll".ExecuParamsSqlOrStored(false).AsList<ProductVM>();
            return View();
        }
        public ActionResult Search()
        {
            var model = "ProductsSelectAll".ExecuParamsSqlOrStored(false).AsList<ProductVM>();

            return PartialView(model);
        }
        [HttpGet]
        public ActionResult Create()
        {

            ProductVM obj = new ProductVM();
            return PartialView("~/Areas/Admin/Views/Product/Create.cshtml", obj);
        }
        [HttpGet]
        public ActionResult Edit(int Id)
        {
            var model = "ProductsSelectById".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<ProductVM>();
            return PartialView("~/Areas/Admin/Views/Product/Create.cshtml", model.FirstOrDefault());
        }
        [HttpPost]

        public ActionResult Create(ProductVM model)
        {




            var data = "ProductsInsert".ExecuParamsSqlOrStored(false, "NameAr".KVP(model.NameAr),
                "NameEn".KVP(model.NameEn), "DescAr".KVP(model.DescAr), "DescEn".KVP(model.DescEn),
                "Price".KVP(model.Price), "Note".KVP(model.Note), 
                "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "Add Success", JsonRequestBehavior.AllowGet);


            }


            return Json("error," + " Error", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int Id)
        {
            //var data = "SP_DeleteCategory".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            ProductVM obj = new  ProductVM();
            obj.Id = Id;
            return PartialView("~/Areas/Admin/Views/Product/Delete.cshtml", obj);



        }
        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "ProductsDelete".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();

            if (data == 1)
                return Json("success," + "Delete Success", JsonRequestBehavior.AllowGet);
            else
                return Json("error," + "Error", JsonRequestBehavior.AllowGet);
        }
    }
}