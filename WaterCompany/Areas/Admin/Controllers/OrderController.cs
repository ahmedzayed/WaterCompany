﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WaterCompany.Core;
using WaterCompanyViewModel.Admin;

namespace WaterCompany.Areas.Admin.Controllers
{
    public class OrderController : Controller
    {
        // GET: Admin/Order
        public ActionResult Index()
        {

            return View();
        }

        public ActionResult Search()
        {
            var model = "GetAllOrder".ExecuParamsSqlOrStored(false).AsList<OrderVM>();

            return PartialView(model);

        }
        public ActionResult Invoice()
        {

            return View();
        }
        public ActionResult AcceptInvoice(int id)
        {
            var data = "UpdateOrder".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<OrderVM>();
            var Offers = "GetAllOrderById".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<OrderFM>();

            int OfferId = Offers.FirstOrDefault().OfferId;
            var OfferDetails= "OffersDetailsSelectAll1Order".ExecuParamsSqlOrStored(false, "OfferId".KVP(OfferId)).AsList<OfferDetailsVM>();

            foreach (var item in OfferDetails)
            {
                var OrderDetailsInsert = "InsertOrderDetails".ExecuParamsSqlOrStored(false, "OrderId".KVP(id),
                              "ProductId".KVP(item.ProductId), "Quntity".KVP(item.Quantity), "Price".KVP(item.Price),
                             
                              "Id".KVP(item.Id)).AsNonQuery();

            }


            return RedirectToAction("Index");
           

        }

        public ActionResult SearchInvicoice()
        {
            var model = "GetAllOrder1".ExecuParamsSqlOrStored(false).AsList<OrderVM>();

            return PartialView(model);

        }
    }
}