﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WaterCompany.Core;
using WaterCompanyViewModel.Admin;

namespace WaterCompany.Areas.Admin.Controllers
{
    public class EmployeeController : Controller
    {
        // GET: Admin/Employee
        public ActionResult Index()
        {
            //var model = "ProductsSelectAll".ExecuParamsSqlOrStored(false).AsList<EmployeeVM>();
            return View();
        }
        public ActionResult Search()
        {
            var model = "EmployeesSelect".ExecuParamsSqlOrStored(false).AsList<EmployeeVM>();

            return PartialView(model);
        }
        [HttpGet]
        public ActionResult Create(int EmployeeId=0)
        {

            EmployeeVM obj = new EmployeeVM();
            var lst2 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst2.AddRange("EmpPositionDrop".ExecuParamsSqlOrStored(false).AsList<EmployeeDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == EmployeeId ? true : false
            }).ToList());
            ViewBag.PositionId = lst2;
            return PartialView("~/Areas/Admin/Views/Employee/Create.cshtml", obj);
        }
        [HttpGet]
        public ActionResult Edit(int Id,int EmployeeId=0)
        {
            EmployeeVM obj = new EmployeeVM();
            var lst2 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst2.AddRange("EmpPositionDrop".ExecuParamsSqlOrStored(false).AsList<EmployeeDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == EmployeeId ? true : false
            }).ToList());
            ViewBag.PositionId = lst2;
            var model = "EmployeesSelectById".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<EmployeeVM>();
            return PartialView("~/Areas/Admin/Views/Employee/Create.cshtml", model.FirstOrDefault());
        }
        [HttpPost]

        public ActionResult Create(EmployeeVM model)
        {
            

            var data = "EmployeesInsert".ExecuParamsSqlOrStored(false, "NameAr".KVP(model.NameAr),
                "NameEn".KVP(model.NameEn),
                "PositionId".KVP(model.PositionId),
                "Telephone".KVP(model.Telephone), "Address".KVP(model.Address),
                 "PercentAmount".KVP(model.PercentAmount),
                
               
                "Note".KVP(model.Note),
                "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "Add Success", JsonRequestBehavior.AllowGet);


            }


            return Json("error," + " Error", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int Id)
        {
            //var data = "SP_DeleteCategory".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            EmployeeVM obj = new EmployeeVM();
            obj.Id = Id;
            return PartialView("~/Areas/Admin/Views/Employee/Delete.cshtml", obj);



        }
        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "EmployeesDelete".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();

            if (data == 1)
                return Json("success," + "Delete Success", JsonRequestBehavior.AllowGet);
            else
                return Json("error," + "Error", JsonRequestBehavior.AllowGet);
        }
    }
}