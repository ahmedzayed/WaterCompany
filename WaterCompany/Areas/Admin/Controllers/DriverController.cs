﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WaterCompany.Core;
using WaterCompanyViewModel.Admin;

namespace WaterCompany.Areas.Admin.Controllers
{
    public class DriverController : Controller
    {
        // GET: Admin/Driver
        #region Actions
        public ActionResult Index()
        {
            //var model = "DriversSelectAll".ExecuParamsSqlOrStored(false).AsList<DriverVM>();
            return View();
        }
        public ActionResult Search()
        {
            var model = "DriversSelect".ExecuParamsSqlOrStored(false).AsList<DriverVM>();

            return PartialView(model);
        }
        [HttpGet]
        public ActionResult Create(int CarId=0,int MangerId=0)
        {
            var lst2 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst2.AddRange("EmpDrop".ExecuParamsSqlOrStored(false).AsList<EmployeeDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == MangerId ? true : false
            }).ToList());
            ViewBag.ManagerId = lst2;

            var lst3 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst3.AddRange("CarDrop".ExecuParamsSqlOrStored(false).AsList<CarDrop>().Select(s => new SelectListItem
            {
                Text = s.TypeCars+s.NumberCars,
                Value = s.Id.ToString(),
                Selected = s.Id == CarId ? true : false
            }).ToList());
            ViewBag.carId = lst3;
            var lst4 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst4.AddRange("Sp_RegionDrop".ExecuParamsSqlOrStored(false).AsList<RegionVM>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.RegionId = lst4;

            DriverFM obj = new DriverFM();
            return PartialView("~/Areas/Admin/Views/Driver/Create.cshtml", obj);
        }
        [HttpGet]
        public ActionResult Edit(int Id ,int MangerId = 0,int CarId=0)
        {
            var lst2 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst2.AddRange("EmpDrop".ExecuParamsSqlOrStored(false).AsList<EmployeeDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == MangerId ? true : false
            }).ToList());
            ViewBag.ManagerId = lst2;

            var lst3 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst3.AddRange("CarDrop".ExecuParamsSqlOrStored(false).AsList<CarDrop>().Select(s => new SelectListItem
            {
                Text = s.TypeCars + s.NumberCars,
                Value = s.Id.ToString(),
                Selected = s.Id == CarId ? true : false
            }).ToList());
            ViewBag.carId = lst3;
            var lst4 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst4.AddRange("Sp_RegionDrop".ExecuParamsSqlOrStored(false).AsList<RegionVM>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.RegionId = lst4;
            var model = "DriversSelectById".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<DriverFM>();
            return PartialView("~/Areas/Admin/Views/Driver/Create.cshtml", model.FirstOrDefault());
        }
        [HttpPost]

        public ActionResult Create(DriverFM model)
        {

            
            var data = "DriversInsert".ExecuParamsSqlOrStored(false, "NameAr".KVP(model.NameAr),
                "NameEn".KVP(model.NameEn), "Telephone".KVP(model.Telephone), "Address".KVP(model.Address),
                "PercentAmount".KVP(model.PercentAmount), "Area".KVP(model.Area), "ManagerId".KVP(model.ManagerId),
                "CarId".KVP(model.CarId),
                "Note".KVP(model.Note),
                                "RegionId".KVP(model.RegionId),

                "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "Add Success", JsonRequestBehavior.AllowGet);


            }


            return Json("error," + " Error", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int Id)
        {
            //var data = "DriversDelete".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            DriverFM obj = new DriverFM();
            obj.Id = Id;
            return PartialView("~/Areas/Admin/Views/Driver/Delete.cshtml", obj);



        }
        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "DriversDelete".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();

            if (data == 1)
                return Json("success," + "Delete Success", JsonRequestBehavior.AllowGet);
            else
                return Json("error," + "Error", JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}