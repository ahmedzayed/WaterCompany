﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WaterCompany.Core;
using WaterCompany.Models;
using WaterCompanyViewModel.Admin;

namespace WaterCompany.Areas.Admin.Controllers
{
    public class InvoiceController : Controller
    {
        // GET: Admin/Invoice
        public List<InProductVm> _ListProduct;

        public ActionResult Index()
        {

            return View();
        }

        public ActionResult Search()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            ApplicationUser currentUser = UserManager.FindById(User.Identity.GetUserId());
            int userId = currentUser.UserId;
            var Role = UserManager.GetRolesAsync(currentUser.Id).Result.FirstOrDefault();


            if (Role == "Admin")
            {
                var model = "InvoicesSelectAll".ExecuParamsSqlOrStored(false).AsList<InvoiceVM>();
                return PartialView(model);

            }
            else
            {

                var model = "InvoicesSelectAllByUser".ExecuParamsSqlOrStored(false,"Id".KVP(userId)).AsList<InvoiceVM>();
                return PartialView(model);
            }

        }
        public ActionResult Invoice()
        {

            return View();

        }

        public ActionResult InvoiceSearch()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            ApplicationUser currentUser = UserManager.FindById(User.Identity.GetUserId());
            int userId = currentUser.UserId;
           var Role=  UserManager.GetRolesAsync(currentUser.Id).Result.FirstOrDefault();

         
            if ( Role== "Admin")
            {
                var model = "Invoices2SelectAll".ExecuParamsSqlOrStored(false).AsList<InvoiceVM>();
                return PartialView(model);

            }
            else
            {
                var model = "Invoices3SelectAll".ExecuParamsSqlOrStored(false,"Id".KVP(userId)).AsList<InvoiceVM>();
                return PartialView(model);
            }


        }

        [HttpGet]
        public ActionResult Create(int DriverId=0,int ProductId=0,int DelegateId=0,int PaymentTypeId=0,int TypeId=0)
        {
            // InvoiceVm obj = new InvoiceVm();

            var lst = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst.AddRange("TypeDrop".ExecuParamsSqlOrStored(false).AsList<EmployeeDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == TypeId ? true : false
            }).ToList());
            ViewBag.TypeId = lst;
            var lst1 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst1.AddRange("paymentTypeDrop".ExecuParamsSqlOrStored(false).AsList<EmployeeDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == PaymentTypeId ? true : false
            }).ToList());
            ViewBag.PaymentTypeId = lst1;

            var lst2 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst2.AddRange("DelegateDrop".ExecuParamsSqlOrStored(false).AsList<EmployeeDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == DelegateId ? true : false
            }).ToList());
            ViewBag.DelegatId = lst2;
            var lst4 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst4.AddRange("DriverDrop".ExecuParamsSqlOrStored(false).AsList<EmployeeDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == DriverId ? true : false
            }).ToList());
            ViewBag.DriverId = lst4;
            var lst5 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst5.AddRange("ProductDrop".ExecuParamsSqlOrStored(false).AsList<EmployeeDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == ProductId ? true : false
            }).ToList());
            ViewBag.ProductId = lst5;

            TempData["InvoiceProduct"] = null;
            //return PartialView("~/Areas/Invoice/Views/ManageInvoice/Create.cshtml");
            return View();
        }

        [HttpPost]
        public JsonResult Create(InvoiceFM viewModel)
        {
            if (viewModel.DelegateId != 0)
            {

                var result = "InvoicesInsert".ExecuParamsSqlOrStored(false, "Date".KVP(viewModel.InvoiceDate),
               "DelegateId".KVP(viewModel.DelegateId), "DriverId".KVP(viewModel.DriverId), "TypeId".KVP(viewModel.TypeId),
               "PaymentId".KVP(viewModel.PaymentId), "Note".KVP(viewModel.Note), 
              
               "Id".KVP(viewModel.Id)).AsNonQuery();
                //if (data != 0)
                //{
                //    return Json("success," + "Add Success", JsonRequestBehavior.AllowGet);
                
                //}

                //string result = _InvoiceService.Save(viewModel);

                if (result != 0)
                {
                    var model = "InvoicesSelectAll".ExecuParamsSqlOrStored(false).AsList<InvoiceVM>().LastOrDefault();
                    var resultDeleteAll = "InvoicesDetailsDeleteAll".ExecuParamsSqlOrStored(false, "InvoiceId".KVP(model.Id)).AsNonQuery();
                    //string resultDeleteAll = _InvoiceService.DeleteProductAll(int.Parse(result.ToString()));
                    _ListProduct = TempData["InvoiceProduct"] as List<InProductVm>;
                    if (_ListProduct != null)
                    {

                        for (int i = 0; i < _ListProduct.Count(); i++)
                        {
                            result="InvoicesDetailsInsertRow".ExecuParamsSqlOrStored(false, "InvoiceId".KVP(model.Id),
               "ProductId".KVP(_ListProduct[i].Id), "Price".KVP(_ListProduct[i].Price), "Quantity".KVP(_ListProduct[i].Quantity), "CreatedBy".KVP(1)
               ).AsNonQuery();
                            //result = _InvoiceService.SaveProduct(result, _ListProduct[i].Id, _ListProduct[i].Price, _ListProduct[i].Quantity);
                        }
                    }
                    return Json("success," + "Add Success", JsonRequestBehavior.AllowGet);
                }
                else
                    return Json("error," + " Error", JsonRequestBehavior.AllowGet);
            }
            return null;

        }
        public decimal getProductPrice(string productId)
        {
            //var res = _commonService.FindProductPrice(int.Parse(productId));
            var model= "ProductPriceSelect".ExecuParamsSqlOrStored(false, "productId".KVP(productId)).AsList<PriceSelect>().FirstOrDefault();
            return (model.Price);

        }

        public ActionResult SearchProductEdit(string RequestId)
        {
            var data = "InvoicesDetailsSelectAll".ExecuParamsSqlOrStored(false, "InvoiceId".KVP(int.Parse(RequestId))).AsList<InvoiceDetailsDetails>();

            //var data = _InvoiceService.FindAllProducts(int.Parse(RequestId == "" ? "0" : RequestId));
            return PartialView("~/Areas/Admin/Views/Invoice/SercheDetails.cshtml", data);

        }

        public ActionResult AcceptInvoice( int id)
        {
            var data = "UpdateInvoice".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<InvoiceFM>();
            return RedirectToAction("Index");

        }
        [HttpPost]

        public JsonResult AddInvoiceProduct(string ProductName, int ProductId, decimal Price, decimal Quantity)
        {
            TempData.Keep();

            //InProductVm objProduct = _InvoiceService.GetProductById(ProductId);
            var objProduct = "ProductsSelectById".ExecuParamsSqlOrStored(false, "Id".KVP(ProductId)).AsList<ProductVM>().FirstOrDefault();

            InProductVm obj = new InProductVm();
            obj.Id = ProductId;
            obj.Price = Price;
            obj.Quantity = Quantity;
            obj.NameAr = ProductName;
            obj.TotalPrice = Price*Quantity;
            //obj.DelegetPercent = (Price * Quantity * objProduct.DelegetPercent) / 100;
            //obj.DriverPercent = (Price * Quantity * objProduct.DriverPercent) / 100;

            if (TempData["InvoiceProduct"] != null)
            {

                _ListProduct = TempData["InvoiceProduct"] as List<InProductVm>;
                _ListProduct.Add(obj);
                TempData["InvoiceProduct"] = _ListProduct;

            }
            else
            {
                _ListProduct = new List<InProductVm>();
                _ListProduct.Add(obj);
                TempData["InvoiceProduct"] = _ListProduct;
            }


            return Json("success," + "Add Success", JsonRequestBehavior.AllowGet);
        }

        public ActionResult SearchProduct(string RequestId)
        {
            TempData.Keep();

            //var data = _InvoiceService.FindAllProducts(int.Parse(RequestId == "" ? "0" : RequestId));
            var data = "InvoicesDetailsSelectAll".ExecuParamsSqlOrStored(false, "InvoiceId".KVP(RequestId)).AsList<InProductVm>();
            if (data.Count() > 0)
            {
                for (int i = 0; i < data.Count(); i++)
                {
                    InProductVm obj = new InProductVm();
                    obj.Id = data[i].Id;
                    obj.Price = data[i].Price;
                    obj.Quantity = data[i].Quantity;
                    obj.TotalPrice = data[i].TotalPrice;

                    obj.NameAr = data[i].NameAr;

                    if (TempData["InvoiceProduct"] != null)
                    {

                        _ListProduct = TempData["InvoiceProduct"] as List<InProductVm>;
                        _ListProduct.Add(obj);
                        TempData["InvoiceProduct"] = _ListProduct;

                    }
                    else
                    {
                        _ListProduct = new List<InProductVm>();
                        _ListProduct.Add(obj);
                        TempData["InvoiceProduct"] = _ListProduct;
                    }

                }
                return PartialView("~/Areas/Admin/Views/Invoice/SearchProduct.cshtml", _ListProduct);
            }
            else
            {
                if (TempData["InvoiceProduct"] != null)
                {
                    _ListProduct = TempData["InvoiceProduct"] as List<InProductVm>;
                }
                else
                {
                    _ListProduct = new List<InProductVm>();
                }

                return PartialView("~/Areas/Admin/Views/Invoice/SearchProduct.cshtml", _ListProduct);
            }

        }

        [HttpPost]

        public ActionResult DeleteInvoiceProduct(int Id)
        {
            TempData.Keep();
            _ListProduct = TempData["InvoiceProduct"] as List<InProductVm>;
         
            InProductVm item = _ListProduct.Where(x => x.Id == Id).FirstOrDefault();
            _ListProduct.Remove(item);
            //TempData.Remove(Id.ToString());
            TempData["InvoiceProduct"] = _ListProduct;
            TempData.Keep();
            //return PartialView("~/Areas/Admin/Views/Invoice/SearchProduct.cshtml", _ListProduct);
            return Json("success," + "Deleted Success", JsonRequestBehavior.AllowGet);

        }
        [HttpGet]
        public ActionResult Edit(string id,int DriverId= 0, int ProductId = 0, int DelegateId = 0, int PaymentTypeId = 0, int TypeId = 0)
        {

            var _viewModel = "InvoicesSelectById".ExecuParamsSqlOrStored(false, "Id".KVP(int.Parse(id))).AsList<InvoiceFMEdit>();
            ViewBag.invoiceId = id;
            var lst = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst.AddRange("TypeDrop".ExecuParamsSqlOrStored(false).AsList<EmployeeDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == TypeId ? true : false
            }).ToList());
            ViewBag.TypeId = lst;
            var lst1 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst1.AddRange("paymentTypeDrop".ExecuParamsSqlOrStored(false).AsList<EmployeeDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == PaymentTypeId ? true : false
            }).ToList());
            ViewBag.PaymentTypeId = lst1;

            var lst2 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst2.AddRange("DelegateDrop".ExecuParamsSqlOrStored(false).AsList<EmployeeDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == DelegateId ? true : false
            }).ToList());
            ViewBag.DelegatId = lst2;
            var lst4 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst4.AddRange("DriverDrop".ExecuParamsSqlOrStored(false).AsList<EmployeeDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == DriverId ? true : false
            }).ToList());
            ViewBag.DriverId = lst4;
            var lst5 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst5.AddRange("ProductDrop".ExecuParamsSqlOrStored(false).AsList<EmployeeDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == ProductId ? true : false
            }).ToList());
            ViewBag.ProductId = lst5;

            // return PartialView("~/Areas/Invoice/Views/ManageInvoice/Create.cshtml", _viewModel);
            return View("~/Areas/Admin/Views/Invoice/Details.cshtml", _viewModel.FirstOrDefault());
        }
    }
}