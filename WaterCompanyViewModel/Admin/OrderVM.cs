﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterCompanyViewModel.Admin
{
   public class OrderVM
    {
        public int Id { get; set; }
        public string OfferName { get; set; }
        public string CustomerName { get; set; }
        public string Note { get; set; }
        public int Quntity { get; set; }
        public DateTime OrderDate { get; set; }
    }
}
