﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterCompanyViewModel.Admin
{
  public  class OffersFM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal Value { get; set; }
        public string Nots { get; set; }
        public int ProductId { get; set; }
        public decimal Price { get; set; }
        public int   Quantity { get; set; }
        public int RequestId { get; set; }

    }
}
