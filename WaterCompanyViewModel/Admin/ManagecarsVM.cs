﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterCompanyViewModel.Admin
{
   public class ManagecarsVM
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string Color { get; set; }
        public string Model { get; set; }
        public string Note { get; set; }
        public string Number { get; set; }

    }
}
