﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterCompanyViewModel.Admin
{
  public  class InvoiceVM
    {
        public int Id { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string Note { get; set; }
        public string Delegate { get; set; }
        public string Driver { get; set; }
        public string InvoiceType { get; set; }
        public string IvType { get; set; }

    }
}
