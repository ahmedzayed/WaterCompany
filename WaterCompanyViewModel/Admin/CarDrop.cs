﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterCompanyViewModel.Admin
{
   public class CarDrop
    {
        public int Id { get; set; }
        public string TypeCars { get; set; }
        public string NumberCars { get; set; }
    }
}
