﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterCompanyViewModel.Admin
{
   public class EmployeeVM
    {
        public int Id { get; set; }
        public string NameAr { get; set; }
        public string NameEn { get; set; }
        public int PositionId { get; set; }
        public string Telephone { get; set; }
        public string Address { get; set; }
        public decimal PercentAmount { get; set; }
        public string Note { get; set; }

    }
}
