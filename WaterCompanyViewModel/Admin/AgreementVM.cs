﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterCompanyViewModel.Admin
{
   public class AgreementVM
    {
        public int Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Area { get; set; }
        public string Delegat { get; set; }
        public string Car { get; set; }
        public string Note { get; set; }
        public string RegionName { get; set; }
    }
}
