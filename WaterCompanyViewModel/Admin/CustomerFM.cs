﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterCompanyViewModel.Admin
{
  public  class CustomerFM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Longid { get; set; }
        public string Latid { get; set; }
        public string Area { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int RegionId { get; set; }
    }
}
